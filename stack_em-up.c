#include<stdio.h>
#define size 1000

struct stack {
    int s[size];
    int top;
    } U, D;

void pushU(int item)
{
    U.top++;
    U.s[U.top] = item;
}
void pushD(int item)
{
    D.top++;
    D.s[D.top] = item;
}
int popU()
{
    int item;
    item = U.s[U.top];
    U.top--;
    return (item);
}
void popD()
{
    int item;
    item = D.s[D.top];
    D.top--;
}
void displayU()
{
    int i;
    for (i= U.top; i>= 0; i--){
        if(i!=U.top)
            printf(" ");
        printf("%d", U.s[i]);
    }
}
void displayD()
{
    int i;
    for (i= D.top; i>= 0; i--){
        if(i!=D.top)
            printf(" ");
        printf("%d", D.s[i]);
    }
}
int main()
{
    int T, N, Q, i, j, n, temp;
    char q;

    scanf("%d", &T);
    for(i=1; i<=T; ++i){
        printf("Case %d:\n", i);
        U.top= -1;
        D.top= -1;
        scanf("%d", &N);
        for(n=N; n>0; --n){
            pushU(n);
        }
        scanf("%d", &Q);

        for(j=0; j<Q; ++j){
            scanf(" %c", &q);

            switch(q){

            case 'W':
                if(U.top==-1)
                    printf("Nothing to wash!\n");
                else{
                    temp=popU();
                    pushD(temp);
                }
                break;
            case 'R':
                if(D.top == -1)
                    printf("Nothing to be placed in shelf!\n");
                else
                    popD();
                break;

            case 'S':
                if(U.top > -1 && D.top > -1){
                    displayU();
                    printf("\n");
                    displayD();
                    printf("\n");
                }
                else if(U.top == -1 && D.top > -1){
                    printf("Nothing to wash!\n");
                    displayD();
                    printf("\n");
                }
                else if(U.top > -1 && D.top == -1){
                    displayU();
                    printf("\n");
                    printf("Nothing to be placed in shelf!\n");
                }
                break;
            }

        }
    }
    return 0;
}

